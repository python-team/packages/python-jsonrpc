python-jsonrpc (1.13.0-5) unstable; urgency=medium

  * Team Upload
  * Set "Rules-Requires-Root: no"
  * Use new dh-sequence-python3
  * Remove vendored six.py

 -- Alexandre Detiste <tchet@debian.org>  Sun, 01 Dec 2024 18:43:32 +0100

python-jsonrpc (1.13.0-4) unstable; urgency=medium

  * Team Upload
  * remove extraneous dep. on python3-mock
  * finish move from nose to pytest (Closes: #1018518)

 -- Alexandre Detiste <tchet@debian.org>  Tue, 26 Mar 2024 09:24:45 +0100

python-jsonrpc (1.13.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dpkg-dev.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 23:27:19 +0100

python-jsonrpc (1.13.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 14:38:31 -0400

python-jsonrpc (1.13.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Andrej Shadura ]
  * New upstream release (Closes: #944250).
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 17 Apr 2020 12:45:23 +0200

python-jsonrpc (1.12.1-1) unstable; urgency=medium

  * New upstream version 1.12.1
  * Bump debhelper compat level to 12.
  * Bump Standards-Version to 4.3.0 (no change).
  * Drop myself from Uploaders.

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 18 Jan 2019 07:30:37 +0000

python-jsonrpc (1.11.1-1) unstable; urgency=low

  [ Ghislain Antony Vaillant ]
  * Initial release. (Closes: #879050)

 -- Mo Zhou <cdluminate@gmail.com>  Mon, 10 Dec 2018 12:46:35 +0000
